//
//  ShowViewController.swift
//  Core-Data-Demo
//
//  Created by Kimheang on 12/6/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import CoreData
class ShowViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var userTable: UITableView!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    var users = [Student]()
    override func viewDidLoad() {
        super.viewDidLoad()
        managedObjectContext = appDelegate.persistentContainer.viewContext
    }
    override func viewWillAppear(_ animated: Bool) {
        users = []
        getUsers()
        userTable.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        cell.textLabel?.text = users[indexPath.row].userName
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "updateScreen") as? UpdateViewController
        {
            vc.name = users[indexPath.row].userName!
            vc.pwd = users[indexPath.row].password!
            vc.age = users[indexPath.row].age!
            vc.user = users[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    func getUsers(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
        request.returnsObjectsAsFaults = false
        do {
            let result = try managedObjectContext.fetch(request)
            for user in result as! [NSManagedObject] {
                users.append(user as! Student)
            }
        } catch {
            print("Failed")
        }
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            managedObjectContext.delete(users[indexPath.row])
            users.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            appDelegate.saveContext()
        }
    }
}
