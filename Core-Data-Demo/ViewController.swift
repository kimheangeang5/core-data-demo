//
//  ViewController.swift
//  Core-Data-Demo
//
//  Created by Kimheang on 12/5/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var userText: UITextField!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    var users = [Student]()
    var newUser : NSManagedObject!
    override func viewDidLoad() {
        super.viewDidLoad()
        managedObjectContext = appDelegate.persistentContainer.viewContext
    }
    @IBAction func addUserTap(_ sender: Any) {
        addUser()
        navigationController?.popViewController(animated: true)
    }
    func addUser() {
        newUser = Student(context: managedObjectContext)
        newUser.setValue(userText.text, forKey: "userName")
        newUser.setValue(passwordText.text, forKey: "password")
        newUser.setValue(ageText.text, forKey: "age")
        userText.text = ""
        passwordText.text = ""
        ageText.text = ""
        userText.becomeFirstResponder()
        appDelegate.saveContext()
    }
}

