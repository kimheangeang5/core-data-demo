//
//  UpdateViewController.swift
//  Core-Data-Demo
//
//  Created by Kimheang on 12/6/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import CoreData
class UpdateViewController: UIViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var userNameText: UITextField!
    var name = ""
    var pwd = ""
    var age = ""
    var user: NSManagedObject!
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameText.text = name
        passwordText.text = pwd
        ageText.text = age
        managedObjectContext = appDelegate.persistentContainer.viewContext
    }
    
    @IBAction func updateButtonTap(_ sender: Any) {
        user.setValue(userNameText.text, forKey: "userName")
        user.setValue(passwordText.text, forKey: "password")
        user.setValue(ageText.text, forKey: "age")
        userNameText.text = ""
        passwordText.text = ""
        ageText.text = ""
        userNameText.becomeFirstResponder()
        navigationController?.popViewController(animated: true)
    }
}
